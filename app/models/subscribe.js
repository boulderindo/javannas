var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var SubscribeSchema = new Schema({
   email: String
});

mongoose.model('Subscribe', SubscribeSchema);