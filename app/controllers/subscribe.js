var express = require('express'),
    app = express(),
    router = express.Router(),
    mongoose = require('mongoose'),
    Subscribe = mongoose.model('Subscribe');

module.exports = function (app){
    app.use('/v1', router);
};


// =====================
//-------------
// Subscriber-Post Subscriber
router.post('/subscribe', function(req, res, next){
    var email_data = new RegExp( req.body.email, "i");
    var data = Subscribe.findOne({email:email_data});
    data.exec(function(err, result){
        if (result == null){
            var inputdata = new Subscribe({email:req.body.email});
            inputdata.save(function(err){
                if (err){
                    console.log(err);
                } else {
                    console.log('input subscriber sukses');
                    res.json({
                        message: "Success"
                    }).status(200);
                }
            })
        } else {
            res.status(409).json({message:"Email sudah terdaftar"})
        }
    })
});

//-------------
// Subscriber-ShowAll Subscriber
router.get('/subscribe', function(req, res, next){
    var data = Subscribe.find({});
    data.exec(function(err, result){
        if(err) return handleError(err);
        res.json({result}).status(200);
    });
});



